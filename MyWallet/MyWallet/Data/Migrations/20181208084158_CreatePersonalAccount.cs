﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyWallet.Data.Migrations
{
    public partial class CreatePersonalAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PensonalAccount",
                table: "AspNetUsers",
                newName: "PersonalAccount");

            migrationBuilder.CreateTable(
                name: "PensonalAccounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Number = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PensonalAccounts", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PensonalAccounts");

            migrationBuilder.RenameColumn(
                name: "PersonalAccount",
                table: "AspNetUsers",
                newName: "PensonalAccount");
        }
    }
}

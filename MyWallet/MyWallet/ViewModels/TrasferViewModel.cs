﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyWallet.ViewModels
{
    public class TrasferViewModel
    {

        public string UserFrom { get; set; }

        [Required]
        [Display(Name = "Лицевой счет")]
        public string UserTo { get; set; }

        [Required]
        [Display(Name = "Сумма")]
        public double Amount { get; set; }
    }
}

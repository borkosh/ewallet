﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyWallet.Data;
using MyWallet.ViewModels;
using MyWallet.Models;

namespace MyWallet.Controllers
{
    public class TransferController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;

        public TransferController(
            ApplicationDbContext context,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public IActionResult SetTransfer(TrasferViewModel model)
        {
            if (ModelState.IsValid)
            {


                //AppUser userTo = _userManager.GetUserNameAsync(_userManager.GetUserName(User));
                string userFromId = _userManager.GetUserId(User);
                string userToId = _userManager.FindByNameAsync(model.UserTo).Result.Id;

                if (userToId != null)
                {
                    if (CheckBalance(userFromId, model.Amount))
                    {
                        AppUser userFrom = _context.AppUsers.FirstOrDefault(f => f.Id == userFromId);
                        userFrom.Balance = userFrom.Balance - model.Amount;
                        _context.AppUsers.Update(userFrom);
                        _context.SaveChanges();

                        AppUser userTo = _context.AppUsers.FirstOrDefault(f => f.Id == userToId);
                        userTo.Balance = userTo.Balance + model.Amount;
                        _context.AppUsers.Update(userTo);
                        _context.SaveChanges();
                    }

                }

                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction("Index", "Home");
        }

        public double  GetBalance(string UserId)
        {
            double balance =_userManager.FindByIdAsync(UserId).Result.Balance ;
            return balance;
        }

        public bool CheckBalance(string userId,double balancefrom)
        {
            double balance = _userManager.FindByIdAsync(userId).Result.Balance;
            if (balance >= balancefrom) { return true; } else { return false; }
        }


    }
}
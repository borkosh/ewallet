﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MyWallet.Data;
using MyWallet.Models;

namespace MyWallet.Models
{
    public class Transaction:Entity
    {
        [ForeignKey("Construction")]
        public string UserId { get; set; }
        public AppUser User { get; set; }

        public DateTime CreateDate { get; set; }

        public double Sum { get; set; }

        public string Contragent { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using MyWallet.Models;

namespace MyWallet.Data
{
    public class AppUser:IdentityUser
    {
        public string Name { get; set; }

        public string PersonalAccount { get; set; }

        public double Balance { get; set; }

    }
}
